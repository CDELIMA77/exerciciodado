package com.company;
import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;

public class Sorteador {

    public int definirFaces() {
        Scanner definirQtdFaces = new Scanner(System.in);

        Impressora.exibirMensagemEntrarFace();
        int qtdFaces = definirQtdFaces.nextInt();
        return qtdFaces;
    }

    public ArrayList<Integer> sortearDadosTresVezes(int qtdFaces) {

        ArrayList<Integer> facesSorteadas = new ArrayList<>();
        limparLista(facesSorteadas);
        int soma = 0;

        for ( int i = 0; i < 3 ; i++ ){
            facesSorteadas.add(sortear(qtdFaces));
            soma += facesSorteadas.get(i);
        }
        facesSorteadas.add(soma);
        return facesSorteadas;
    }

    public int sortear(int qtdFaces) {
        Random sortear = new Random();
        Dado dado = new Dado(qtdFaces);

        int i;
        for (i = 0; i < 3; i++) {
                dado.face = sortear.nextInt(qtdFaces) + 1;
            }
        return dado.face;
    }

    public void limparLista(ArrayList<Integer> lista) {
        lista.clear();
    }
}
