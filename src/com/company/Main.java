package com.company;

import java.util.ArrayList;

public class Main {

    //Versao 1 : Criar um sistema que simula o funcionamento de um dado de 6 faces. Ao iniciar o programa
    // deve-se sortear um numero aleatoriamente e exibi-lo no console
    //OK
    //Versao 2: Aprimorar o sistema para que seja possivel utilizar dados com um numero customizavel de faces.
    // A determinação da quantidade de faces deve ser feito dentro do código fonte
    //OK
    //Versao 3: Aprimorar o sistema para que, em cada execução, o dado seja sorteado 3 vezes. O console deve exibir o resultado
    //de cada sorteio e a soma de todos ao fim. Ex 1,5,3,9
    //OK
    //Versao 4: Aprimorar o sistema para que em cada execução o dado seja sorteado em 3 grupos de 3 vezes.

    public static void main(String[] args) {
        Sorteador sorteador = new Sorteador();
        int qtdDefinida;
        ArrayList<Integer> listaSorteados = new ArrayList<>();

        qtdDefinida = sorteador.definirFaces();
        listaSorteados = sorteador.sortearDadosTresVezes(qtdDefinida);
        Impressora.imprimirLista(listaSorteados);
    }
}
